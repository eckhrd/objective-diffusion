Objective Diffusion
===================

Objective Diffusion is a toy program that I write to practise object-oriented
programming with guidelines from Robert C. Martin's book *Clean Code* [^1].
The program solves a discretized form of the one-dimensional heat equation.

The guidelines I strive to adhere to in this repository are:

- Use expressive variable and method **names** to make the code readable
  and self-explanatory and to make comments largely unnecessary.
- Use **code comments** only to give context and rationale. Only
  explain code (variables, objects, methods, algorithms) if you are unable to
  convey its meaning by sensible naming.
- Avoid duplication of code and concepts by adhering to the following
  principles:

  - **Classes** should be

    - small,
    - compact, i.e. class methods should relate to most of the class member
      variables, and
    - serve only purpose (single responsibility principle), in order to make
      the code modular and exchangeable.

  - **Methods** should be small and represent only one level of abstraction.


TODO
----
- develop unit and integration tests
- add and handle exceptions
- add netCDF data output
  
[^1]: Robert C. Martin (2009). *Clean Code: A Handbook of Agile Software
Craftsmanship.* Prentice Hall.
