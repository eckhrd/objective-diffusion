from objective_diffusion.numerics import Model
from pytest import approx

RELATIVE_TOLERANCE = 1e-15


def test_model_consistent_grid_parameters():
    model = Model(
        domain_length=1.0,
        number_of_gridpoints=32,
        simulation_time=1.0,
        cfl_number=0.45
    )
    assert model.dx * model.nx_global == approx(model.Lx, rel=RELATIVE_TOLERANCE)
