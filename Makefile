.PHONY: all clean run test

all: clean test run

run:
	mpirun -n 4 python3 ./main.py

test:
	python3 -m pytest

clean:
	rm -f diffusion.png
	rm -rf objective_diffusion/__pycache__
