from matplotlib import pyplot
from os import path

FIGURE_FILE = './diffusion.png'


class PlottingInterface(object):
    def __init__(self, integrator):
        self.integrator = integrator
        self.x = self.integrator.x_coordinates

    def plot(self):
        y = self.integrator.current_solution
        time = self.integrator.current_time
        pyplot.plot(self.x, y, "o-", label="t = %.2f s" % time)

    def show_plot(self):
        pyplot.legend()
        pyplot.show()

    def save_plot(self):
        pyplot.legend()
        print(f'Saving plot as {path.abspath(FIGURE_FILE)}')
        pyplot.savefig(FIGURE_FILE)
