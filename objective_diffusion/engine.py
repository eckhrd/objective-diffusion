from argparse import ArgumentParser
from .numerics import Model, EulerIntegrator, RKIntegrator
from .plotting import PlottingInterface

DEFAULT_CFL_NUMBER = 0.5
DEFAULT_NUMBER_GRIDPOINTS = 32
DEFAULT_DOMAIN_LENGTH = 1.0
DEFAULT_TIME_INTEGRATION = "rk3"
DEFAULT_SIMULATION_TIME = 1.0

class Engine(object):
    def __init__(self):
        self.__define_command_line_arguments__()
        self.__parse_command_line__()
        self.__init_engine_with_command_line_arguments__()

    def __define_command_line_arguments__(self):
        self.parser = ArgumentParser(description="Integrate the diffusion equation.")
        self.parser.add_argument(
            "-i",
            dest="time_integration_scheme",
            default=DEFAULT_TIME_INTEGRATION,
            type=str,
            help=f"set the time integration scheme ('euler' or 'rk3', default: '{DEFAULT_TIME_INTEGRATION}')",
        )
        self.parser.add_argument(
            "-L",
            dest="domain_length",
            default=DEFAULT_DOMAIN_LENGTH,
            type=float,
            help=f"set the length of the model domain [m] (default: {DEFAULT_DOMAIN_LENGTH})",
        )
        self.parser.add_argument(
            "-nx",
            dest="number_of_gridpoints",
            default=DEFAULT_NUMBER_GRIDPOINTS,
            type=int,
            help=f"set the number of grid points (default: {DEFAULT_NUMBER_GRIDPOINTS})",
        )
        self.parser.add_argument(
            "-t",
            dest="simulation_time",
            default=DEFAULT_SIMULATION_TIME,
            type=float,
            help=f"set the duration for the simulation (simulated time) [s] (default: {DEFAULT_SIMULATION_TIME})",
        )
        self.parser.add_argument(
            "-cfl",
            dest="cfl_number",
            default=DEFAULT_CFL_NUMBER,
            type=float,
            help=f"set the diffusion CFL number [-] (0.5 is the stability limit. default: 0.45{DEFAULT_CFL_NUMBER})",
        )

    def __parse_command_line__(self):
        self.args = self.parser.parse_args()

    def __init_engine_with_command_line_arguments__(self):
        self.model = Model(
            domain_length=self.args.domain_length,
            number_of_gridpoints=self.args.number_of_gridpoints,
            simulation_time=self.args.simulation_time,
            cfl_number=self.args.cfl_number
        )
        self.integrator = self.__select_integrator__(self.args.time_integration_scheme)

        if self.model.mpi.rank == 0:
            self.plotter = PlottingInterface(self.integrator)

    def __select_integrator__(self, time_integration_scheme):
        if time_integration_scheme == "euler":
            return EulerIntegrator(self.model)
        elif time_integration_scheme == "rk3":
            return RKIntegrator(self.model)
        else:
            print(
                "Time integration scheme '%s' not recognized. Please specify 'rk3' or 'euler'."
                % time_integration_scheme
            )
            print("Falling back to 'rk3'...")
            return RKIntegrator(self.model)

    def run(self):
        print(f"Running on a grid of {self.model.nx_global} points.")
        print(f"Using the {self.integrator.name} method.")
        print(f"Integrating for {self.model.nt + 1} timesteps...")
        if self.model.mpi.rank == 0:
            self.plotter.plot()
        self.integrator.integrate()
        print("...done.")
        if self.model.mpi.rank == 0:
            self.plotter.plot()
            self.plotter.save_plot()
