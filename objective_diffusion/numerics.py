import numpy

from .parallel import Messenger


ONE_THIRD = 1.0 / 3.0
TWO_THIRDS = 1.0 - ONE_THIRD
NUMBER_OF_GHOSTCELLS = 1
DIFFUSION_COEFFICIENT = 0.01

class Model(object):
    """
    This class represents the mathematical model of the diffusion problem.

    It contains physical and numerical parameters as well as the numerical grid
    and the discretized tendency function. It does not contain the state of the
    state of the model, such as current time, solution or tendency vectors and
    so on. This information is stored in the numerical integrator instead.

    In this class, I prefer short variable names over more expressive long names
    in order to make mathematical formulae more recognizable.
    """

    def __init__(
            self,
            domain_length=None,
            number_of_gridpoints=None,
            simulation_time=None,
            cfl_number=None
    ):
        self.Lx = domain_length
        self.nx_global = number_of_gridpoints
        self.t_end = simulation_time
        self.cfl = cfl_number

        self.alpha = DIFFUSION_COEFFICIENT
        self.ng = NUMBER_OF_GHOSTCELLS
        self.x0_global = 0.0
        self.x1_global = self.Lx

        self.dx = self.Lx / self.nx_global
        self.dt = max_diffusion_timestep(
            self.dx,
            alpha=self.alpha,
            cfl=self.cfl)
        self.factor = self.alpha / (self.dx * self.dx)
        self.x_global = numpy.linspace(
            self.x0_global + 0.5 * self.dx - self.ng * self.dx,
            self.x1_global - 0.5 * self.dx + self.ng * self.dx,
            self.nx_global + 2 * self.ng
        )
        self.nt = number_of_timesteps(self.t_end, self.dt)

        # parallelization and local grid
        self.mpi = Messenger()
        self.nx = int(round(self.Lx / self.dx / self.mpi.size))
        self.x0 = self.mpi.rank * self.Lx / self.mpi.size\
                  + 0.5*self.dx - self.ng*self.dx
        self.x1 = self.x0 + (self.nx - 1 + 2*self.ng) * self.dx
        self.x = numpy.linspace(self.x0, self.x1, self.nx + 2*self.ng)

        # internal field indices, excluding boundary conditions/ghost points
        self.i_start = self.ng
        self.i_stop = self.ng + self.nx

        # initial condition, global and local
        self.u_0 = -numpy.cos(2.0 * numpy.pi * self.x / self.Lx)

        self.u_global = None
        if self.mpi.rank == 0:
           self.u_global = numpy.empty([self.mpi.size * self.nx])

        self.mpi.comm.Gather(
            self.u_0[self.i_start:self.i_stop],
            self.u_global,
            root = 0
        )

        ## set internal (MPI) and external boundary conditions.
        self.mpi.synchronise(self.u_0, self.nx, self.ng)
        self.set_neumann_boundary_conditions(self.u_0)

    def get_diffusion_tendency(self, u):
        """
        Returns the rate of change of the scalar u due to diffusion
        """
        return self.factor * (
                u[self.i_start-1:self.i_stop-1]
                - 2.0 * u[self.i_start:self.i_stop]
                + u[self.i_start+1:self.i_stop+1]
            )

    def set_neumann_boundary_conditions(self, u):
        if self.mpi.rank == 0:
            u[:self.i_start] = u[self.i_start]

        if self.mpi.rank == self.mpi.size - 1:
            u[self.i_stop:] = u[self.i_stop-1]

    def fit_last_timestep(self, current_integrator_time):
        self.dt = self.t_end - current_integrator_time


class Integrator(object):
    def __init__(self, model):
        self.model = model
        self._t = 0.0
        self.critical_cfl = None
        self.number_of_tendencies = None

    def __init_data_arrays__(self):
        number_of_levels = self.number_of_tendencies + 1
        self.u = numpy.tile(self.model.u_0, (number_of_levels, 1))
        self.u_tend = numpy.zeros((self.number_of_tendencies, self.model.u_0.size))

    def integrate(self):
        for step in range(self.model.nt):
            self.advance_one_timestep()

        self.model.fit_last_timestep(current_integrator_time=self._t)
        self.advance_one_timestep()

        # assemble global solution
        self.model.mpi.comm.Gather(
            self.u[0, self.model.i_start:self.model.i_stop],
            self.model.u_global,
            root=0
        )

    def advance_one_timestep(self):
        self.__apply_model_tendencies__()
        self.__update_to_next_timelevel__()

    def __apply_model_tendencies__(self):
        raise NotImplementedError

    def __update_to_next_timelevel__(self):
        self.u[0, :] = self.u[-1, :]
        self._t += self.model.dt

    @property
    def name(self):
        return self._name

    @property
    def x_coordinates(self):
        return self.model.x_global[self.model.ng:self.model.ng+self.model.nx_global].copy()

    @property
    def current_solution(self):
        return self.model.u_global.copy()

    @property
    def current_time(self):
        return self._t


class RKIntegrator(Integrator):
    """
    This class implements the third-order total-variation-diminishing (TVD)
    Runge-Kutta method — SPP (3,3), see e.g. Spiteri and Ruuth (2002).

    - Spiteri, R. and Ruuth, S. (2002). A new class of optimal high-order
    strong-stability-preserving time discretization methods. SIAM Journal on
    Numerical Analysis, 40 (2): 469-491.
    URL epubs.siam.org/sinum/resource/1/sjnaam/v40/i2/p469_s1.
    """

    def __init__(self, model):
        super().__init__(model)
        self.number_of_tendencies = 3
        self.__init_data_arrays__()
        self._name = "SPP (3,3) third-order TVD Runge-Kutta"

    def __apply_model_tendencies__(self):
        self.u_tend[0, self.model.i_start:self.model.i_stop] = self.model.get_diffusion_tendency(self.u[0, :])
        self.u[1, self.model.i_start:self.model.i_stop] = \
            self.u[0, self.model.i_start:self.model.i_stop] \
            + self.model.dt * self.u_tend[0, self.model.i_start:self.model.i_stop]
        self.model.mpi.synchronise(self.u[1], self.model.nx, self.model.ng)
        self.model.set_neumann_boundary_conditions(self.u[1])

        self.u_tend[1, self.model.i_start:self.model.i_stop] = self.model.get_diffusion_tendency(self.u[1, :])
        self.u[2, self.model.i_start:self.model.i_stop] = (
                0.75 * self.u[0, self.model.i_start:self.model.i_stop]
                + 0.25 * self.u[1, self.model.i_start:self.model.i_stop]
                + 0.25 * self.model.dt * self.u_tend[1, self.model.i_start:self.model.i_stop]
        )
        self.model.mpi.synchronise(self.u[2], self.model.nx, self.model.ng)
        self.model.set_neumann_boundary_conditions(self.u[2])

        self.u_tend[2, self.model.i_start:self.model.i_stop] = self.model.get_diffusion_tendency(self.u[2])
        self.u[3, self.model.i_start:self.model.i_stop] = (
                ONE_THIRD * self.u[0, self.model.i_start:self.model.i_stop]
                + TWO_THIRDS * self.u[2, self.model.i_start:self.model.i_stop]
                + TWO_THIRDS * self.model.dt * self.u_tend[2, self.model.i_start:self.model.i_stop]
        )
        self.model.mpi.synchronise(self.u[3], self.model.nx, self.model.ng)
        self.model.set_neumann_boundary_conditions(self.u[3])


class EulerIntegrator(Integrator):
    """
    This class implements the Euler method - the simplest, first-order accurate
    time integration scheme.

    In addition to the temporal integration scheme, instances of this class contain the
    arrays for storing the state of the corresponding model, i.e. the solution
    vector before and after a time step and the tendency vector, all of which
    are updated every time iteration.

    In this class, I prefer short variable names over more expressive long names
    in order to make mathematical formulae more recognizable.
    """

    def __init__(self, model):
        super().__init__(model)
        self.number_of_tendencies = 1
        self.__init_data_arrays__()
        self._name = "Euler"

    def __apply_model_tendencies__(self):

        self.u_tend[0, self.model.i_start:self.model.i_stop] = self.model.get_diffusion_tendency(self.u[0, :])
        self.u[1, self.model.i_start:self.model.i_stop] = \
            self.u[0, self.model.i_start:self.model.i_stop] \
            + self.model.dt * self.u_tend[0, self.model.i_start:self.model.i_stop]
        self.model.mpi.synchronise(self.u[1], self.model.nx, self.model.ng)
        self.model.set_neumann_boundary_conditions(self.u[1, :])


def max_diffusion_timestep(dx, alpha=1.0, cfl=0.45):
    return cfl * dx**2 / alpha


def number_of_timesteps(duration=None, timestep=None):
    """
    This function returns the number of time steps just before
    'duration' is reached. This is for the main loop to be able to
    explicitly integrate the last time step with correct step size
    needed to hit 'duration' exactly without checking every loop.
    """
    return int(duration / timestep)
