from mpi4py import MPI


class Messenger(object):

    def __init__(self):
        self.comm = MPI.COMM_WORLD
        self.rank = self.comm.Get_rank()
        self.size = self.comm.Get_size()

        self.left_rank = self.rank - 1
        self.right_rank = self.rank + 1

        # Set MPI neighbour tasks to PROC_NULL (= no communication) if they
        # are model boundaries.
        if self.left_rank < 0:
            self.left_rank = MPI.PROC_NULL
        if self.right_rank > self.size - 1:
            self.right_rank = MPI.PROC_NULL


    def synchronise(self, u, nx, ng):
        """
        First, send the left end of the array to the left rank and receive the right ghosts
        from the right rank; then communicate the other way around. This uses Sendrecv() —
        a blocking communication — so if all ranks send the right end of the array, they have
        to be ready to receive into the left buffer at the same time.

        Example with ng = 2 (two ghost cells, --) and nx = 8 (eight grid cells, ++++++++):

              --++++++++--                  left rank, i - 1
                        ▲▲
                        ││
                      --++++++++--          rank, i   (send left, receive right)
                                ▲▲
                                ││
                              --++++++++--  right rank, i + 1
        """

        send_buffer = u[ng:2 * ng]
        recv_buffer = u[ng + nx:]
        self.comm.Sendrecv(
            send_buffer,
            self.left_rank,
            99,
            recv_buffer,
            self.right_rank
        )

        send_buffer = u[nx:ng + nx]
        recv_buffer = u[:ng]
        self.comm.Sendrecv(
            send_buffer,
            self.right_rank,
            101,
            recv_buffer,
            self.left_rank,
        )