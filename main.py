#!/usr/bin/env python3

from objective_diffusion.engine import Engine

if __name__ == "__main__":
    engine = Engine()
    engine.run()
